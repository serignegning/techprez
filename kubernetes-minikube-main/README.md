# kubernetes-minikube techprez
Minikube est un outil qui vous permet d'exécuter Kubernetes localement.
minikube exécute un cluster Kubernetes à nœud unique sur votre ordinateur personnel.

##  Installation Docker

### installation pour Mac, Windows 10 Pro, Enterprise, or Education

https://www.docker.com/get-started

Choisissez Docker Desktop

### installation pour Windows home

https://docs.docker.com/docker-for-windows/install-windows-home/

## Installation Kuberntes Minikube 

https://minikube.sigs.k8s.io/docs/start/

Minikube fournit un tableau de bord (portail Web). 
Accédez au tableau de bord à l'aide de la commande suivante :
 
minikube dashboard

## Récuperer le projet kubernetes-minikube-main dans le répertoire: https://gitlab.com/serignegning/techprez/-/tree/main/kubernetes-minikube-main
Ce projet contient un service Web codé en Java.
Ensuite, déplacez-vous dans le répertoire  avec cd kubernetes-minikube-main/MyService où se trouve le DockerFile.

## Tester le projet en utilisant Docker

Tout d'abord compiler le projet Java: 
* ./gradlew build   under Linux
* gradlew build     under Windows

Builder l'image docker avec la commande suivante:

 docker build -t my-service .
 
 Nb: le point est très important ça spécifie que le dockerfile est dans le repertoire courant

Vérifier que l'image a bien été créé avec la commande suivante: 
docker images

Démarrer le container avec la commande suivante: 

docker run -p 4000:8080 -t my-service

8080 est le port du service Web, 4000 est le port d'accès au conteneur. 

Testez le service Web à l'aide d'un navigateur Web via le lien: http://localhost:4000 

Il affichera "Hello covea !".

Notons bien que on a notre service web et le container Docker dans lequel il est.

Pour arrêter le service Web un Ctr-c suffira dans l'invite de commande.
Parconte pour stopper le container docker on aura besoin de son identifiant.

Récuperer l'ID du conteneur avec la commande : docker ps

Arrêtez le conteneur avec la commande : docker stop containerID

## Maintenant publier votre image docker dans le Docker Hub

Récupérer l'ID de l'image avec la commande: docker images

Taguer l'image docker avec la commande: docker tag imageID yourDockerHubName/imageName:version

Example: docker tag dc9c701d6625 gning10/my-service:1

Pour se connecter à docker hub: 
* docker login      ou
* docker login http://hub.docker.com    ou
* docker login -u username -p password

Pushons l'image dans le docker hub avec la commande: docker push yourDockerHubName/imageName:version

Example: docker push gning10/my-service:1

## Création d'un déploiement kubernetes à partir d'une image Docker
D'abord démarrer minikube si ce n'est pas déjà fait via la commande: minikube start

Ensuite vérifier les nodes via la commande suivante:
kubectl get nodes

kubectl create deployment myservice --image=gning10/my-service:1 

L'image utilisée vient de Docker hub: https://hub.docker.com/r/gning10/my-service/tags c'est l'image créé précédemment
Mais vous pouvez utiliser votre propre image.

Vérification des pods: kubectl get pods

Vérifier si son état est running ou non.

Pour obtenir les logs complets des pods c'est via la commande: kubectl describe pods

Récupérez l'adresse IP mais notez que cette adresse IP est éphémère puisqu'un pod peut être supprimé et remplacé par un nouveau.

En fait, le conteneur Docker est exécuté dans un pod Kubernetes (regardez le pod dans le tableau de bord) via la commande: minikube dashboard

Vous pouvez également entrer à l'intérieur du conteneur en mode interactif avec la commande : kubectl exec -it podname-rthr5 -- /bin/bash

Ou podname est le nom du pod obtenue via la commande: kubectl get pods

Lister le contenue du container avec: ls

Et n'oubliez pas de quitter le container avec: exit

## Exposer le déploiement via un service
Un service Kubernetes est une abstraction qui définit un ensemble logique de pods exécutés quelque part dans le cluster,
qui offrent tous les mêmes fonctionnalités.
Une fois créé, chaque service se voit attribuer une adresse IP unique (également appelée clusterIP).
Cette adresse est liée à la durée de vie du service et ne changera pas tant que le service est actif.

## Exposer les routes HTTP et HTTPS de l'extérieur du cluster pour les services du cluster

Pour certaines parties de notre application (par exemple, le frontend), on souhaite parfois exposer un service sur une adresse IP externe, en dehors de notre cluster.

Les types de service Kubernetes nous permettent de spécifier le type de service que nous souhaitons. La valeur par défaut est ClusterIP.


Les valeurs de type et leurs comportements sont :

* ClusterIP: Expose le service sur une adresse IP interne au cluster. Le choix de cette valeur rend le service uniquement accessible depuis le cluster. Il s'agit du ServiceType par défaut.
* NodePort: Expose le service sur l'IP de chaque nœud à un port statique (le NodePort). Un service ClusterIP, vers lequel le service NodePort est acheminé, est automatiquement créé. Vous pourrez contacter le service NodePort, depuis l'extérieur du cluster, en demandant NodeIP:NodePort.
* LoadBalancer: Expose le service en externe à l'aide de l'équilibreur de charge d'un fournisseur de cloud. Les services NodePort et ClusterIP, vers lesquels les routes de l'équilibreur de charge externe sont automatiquement créés.
* ExternalName: Associe le service au contenu du champ externalName (par exemple, kubernetes.gmf.example.com), en renvoyant un enregistrement CNAME.

## Exposer des routes HTTP et HTTPS en utilisant NodePort
Via la commande
kubectl expose deployment myservice --type=NodePort --port=8080

Récuperer l'address de votre service avec la commande: minikube service myservice --url



## Suppression des resources 

kubectl delete services myservice

kubectl delete deployment myservice

## Scaling et load balancing

Vérifiez si le déploiement myservice est en cours d'exécution :


kubectl get deployments  


Combien d'instances sont réellement en cours d'exécution :

kubectl get pods 

Pour démarrer une seconde instance:

kubectl scale --replicas=2 deployment/myservice

kubectl get deployments

et 

kubectl get pods 

Encore vérifier que la seconde instance a bien été créé.

## Création d'un Service de type LoadBalancer

Vérifiez si le déploiement myservice est en cours d'exécution :

kubectl get deployments  

kubectl expose deployment myservice --type=LoadBalancer --port=8080

minikube service myservice --url

Pour récuperer l'url d'accès à notre application.

## Création d'un déploiement et d'un service de load balancing en utilisant un fichier .yaml

Les fichiers Yaml peuvent être utilisés au lieu d'utiliser la commande "kubectl create deployment" et "kubectl expose deployment"

Le fichier yaml pour le déploiement: https://gitlab.com/serignegning/techprez/-/blob/main/kubernetes-minikube-main/myservice-deployment.yml 

Le fichier yaml pour le service node port:  https://gitlab.com/serignegning/techprez/-/blob/main/kubernetes-minikube-main/myservice-service.yml

Le fichier yaml pour le service de load balancing: https://gitlab.com/serignegning/techprez/-/blob/main/kubernetes-minikube-main/myservice-loadbalancing-service.yml

Pour déployer c'est la commande: kubectl apply -f myservice-deployment.yml

Pour le node port: kubectl apply -f myservice-service.yml

ou 

Pour le service de type load balancing: kubectl apply -f myservice-loadbalancing-service.yml

Testez ensuite si cela fonctionne comme prévu.